from flask import Blueprint
from flask_restful import Resource, Api

greeting = Blueprint('greeting_v2', __name__)
greeting_api = Api(greeting)


class Greeting(Resource):
    def get(self, message, name):
        return {
            'name': name,
            'message': message
        }


greeting_api.add_resource(Greeting, '/greeting/<string:message>/<string:name>')
