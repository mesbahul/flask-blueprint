from flask import Flask

from views.hello import tree_mold

from apis.v1.greeting import greeting as greeting_v1
from apis.v2.greeting import greeting as greeting_v2

if __name__ == '__main__':
    app = Flask(__name__)

    # these blueprint works with different url prefix
    app.register_blueprint(tree_mold, url_prefix="/oak")
    app.register_blueprint(tree_mold, url_prefix="/fir")
    app.register_blueprint(tree_mold, url_prefix="/ash")

    # these blueprint works with only one url prefix
    app.register_blueprint(greeting_v1, url_prefix="/v1")
    app.register_blueprint(greeting_v2, url_prefix="/v2")

    # NOTE: if uncomment followings, then error: "flask-restful blueprints can only be registered once"
    # app.register_blueprint(greeting_v1, url_prefix="/v3")
    # app.register_blueprint(greeting_v2, url_prefix="/v4")

    app.run(host='0.0.0.0', port=5004, debug=True)